﻿using System;
using System.Diagnostics;
using System.Threading;
using AutomationCourseFramework.WebDriver;

namespace AutomationCourseFramework.Pages
{
    public class BasePage : WebDriverBase
    {
        public static void WaitForCondition(Func<bool> condition, string message)
        {
            WaitForCondition(condition, message, TimeSpan.FromSeconds(60), TimeSpan.FromMilliseconds(500));
        }

        public static void WaitForCondition(Func<bool> condition, string message, TimeSpan waitingTime, TimeSpan interval)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            while (!condition())
            {
                if (stopwatch.Elapsed.TotalSeconds > waitingTime.TotalSeconds)
                    throw new Exception(message);
                Thread.Sleep(interval);
            }
        }
    }
}
