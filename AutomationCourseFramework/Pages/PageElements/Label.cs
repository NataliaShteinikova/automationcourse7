﻿using AutomationCourseFramework.WebDriver;
using OpenQA.Selenium;

namespace AutomationCourseFramework.Pages.PageElements
{
    public class Label : BaseElement
    {
        public Label(By locator) : base(locator)
        {
        }
    }
}
