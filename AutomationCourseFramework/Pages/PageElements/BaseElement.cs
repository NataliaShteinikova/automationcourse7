﻿using AutomationCourseFramework.WebDriver;
using OpenQA.Selenium;

namespace AutomationCourseFramework.Pages.PageElements
{
    public abstract class BaseElement : WebDriverBase
    {
        protected By Locator;
        protected BaseElement(By locator)
        {
            Locator = locator;
        }

        public string GetAttributeValue(string attributeName)
        {
            return WebDriver.FindElement(Locator).GetAttribute(attributeName);
        }

        public string Text => WebDriver.FindElement(Locator).Text;

        public void Click()
        {
            WebDriver.FindElement(Locator).Click();
        }
    }
}
