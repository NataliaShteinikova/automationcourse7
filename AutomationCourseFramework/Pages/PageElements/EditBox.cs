﻿using OpenQA.Selenium;

namespace AutomationCourseFramework.Pages.PageElements
{
	public class EditBox : BaseElement
    {
		internal EditBox(By locator) : base(locator)
        {
        }

        public void KeyPress(string value)
        {
			WebDriver.FindElement(Locator).SendKeys(value);
        }

        public void Clear()
        {
            WebDriver.FindElement(Locator).Clear();
        }

        public void ClearAndKeyPress(string value)
        {
            Clear();
            KeyPress(value);
        }
    }
}
