﻿using System;
using OpenQA.Selenium;
using AutomationCourseFramework.Pages.PageElements;

namespace AutomationCourseFramework
{
	public class GridActionButton : Button
	{
		By _locator;
		public GridActionButton(string action) : base(By.Id(action))
		{
			_locator = By.Id(action);
		}

		public string Title => WebDriver.FindElement(_locator).Text;
	}

}
