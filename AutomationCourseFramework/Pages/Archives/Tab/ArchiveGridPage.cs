﻿using System;
using System.Linq;
using OpenQA.Selenium;

namespace AutomationCourseFramework.Pages.Archives.Tab
{
    public class ArchiveGridPage : BasePage
    {

        public ArchiveAsset[] GetArchiveAssets() //GetArchiveAssets(int waitForCount)
        {
            const string assetsXpath = "//div[@id='thumbnailWrapper']/ul[@class='thumbnails']/li";
            //WaitForCondition(() =>
            //{
            //    WaitForDisplay();
            //    if (WebDriver.FindElements(By.XPath(assetsXpath)).Count >= waitForCount) return true;

            //    Browser.Refresh();
            //    WaitForDisplay();
            //    return false;
            //}, $"Assets appearance in the ammount '{waitForCount}' timeout expired");


            return WebDriver
                .FindElements(By.XPath(assetsXpath))
                .Select((el, i) => new ArchiveAsset(i + 1))
                .ToArray();
        }

        public ArchiveAsset GetArchiveAsset(string assetName)
        {
            //WaitForCondition(() =>
            //{
            //    WaitForDisplay();
            //    if (GetArchiveAssets(1).Any(asset => asset.Name == assetName)) return true;

            //    Browser.Refresh();
            //    return false;
            //}, $"Asset with name {assetName} appearance timeout expired on the archive details page");

            //return GetArchiveAssets(1).First(asset => asset.Name == assetName);

            var asseet = GetArchiveAssets().FirstOrDefault(asset => asset.Name == assetName);
            if(asseet == null)
                throw new Exception($"Asset with name {assetName} appearance was not found on the archive details page");

            return asseet;
        }
    }
}
