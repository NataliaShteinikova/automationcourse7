﻿using System;
using AutomationCourseFramework.Pages;
using OpenQA.Selenium;

namespace AutomationCourseFramework
{
	public class AssetDetailPage : BasePage
	{
		public AssetDetailPage()
		{
			
		}

		public int Rating
		{
			get
			{
				var _class = WebDriver.FindElement(By.XPath("//div[@id='preview-title']//span[contains(@class,'rating-info rate')]")).GetAttribute("class"); // TODO
				return (int)Char.GetNumericValue(_class.ToCharArray()[_class.Length - 1]);
			}
			set
			{
				WebDriver.FindElement(By.XPath("//div[@id='preview-title']//span[contains(@class,'rating-info rate')]")).FindElement(By.XPath($"/i[@data-rating={value}]")).Click();
			}
		}
	}
}
