﻿using System;
using System.Diagnostics;
using AutomationCourseFramework.Pages;
using AutomationCourseFramework.Pages.Archives;
using OpenQA.Selenium;

namespace AutomationCourseFramework
{
	public class GridActionPanel : BasePage
	{
		public enum AssetStatus { NotSet, None = 0, High, Two, Three, Four, Normal, Six, Seven, Low };

		public GridActionPanel()
		{
			WaitForActionPanelIsVisible();
		}

		internal void WaitForActionPanelIsVisible()
		{
			WaitForCondition(() => WebDriver.FindElement(By.Id("grid-action-panel-wrapper")).
				GetAttribute("Class").Contains("grid-action-panel-open"),
				"Timeout of waiting for visibility of Action panel expired.");
		}

		internal void WaitForActionPanelIsHide()
		{
			WaitForCondition(() => !WebDriver.FindElement(By.Id("grid-action-panel-wrapper")).
				GetAttribute("Class").Contains("grid-action-panel-open"),
				"Timeout of waiting for visibility of Action panel expired.");
		}

		public void SetRating(int stars)
		{
			if (stars < 1 || stars > 5)
			{
				throw new Exception($"Invalid rating {stars}");
			}
			WebDriver.FindElement(By.Id("setRatingAction")).Click();
			WebDriver.FindElement(By.XPath($"//div[@id='js-rating-buttons']/button[@data-value='{stars}']")).Click();
			WaitForCondition(
				() => WebDriver.FindElement(By.XPath("//div[contains(@class, 'popover')]")) != null,
				"Waited for popover for too long, it never showed up, gave up"
			);
		}

		public void SetStatus(AssetStatus status)
		{
			string sStatus;
			if (status == AssetStatus.NotSet)
			{
				sStatus = "none";
			}
			else
			{
				sStatus = status.ToString();
			}
			WebDriver.FindElement(By.Id("setStatusAction")).Click();
			WebDriver.FindElement(By.XPath($"//div[@id='js-status-buttons']/button[@data-value='{sStatus}']")).Click();
			WaitForCondition(
				() => WebDriver.FindElement(By.XPath("//div[contains(@class, 'popover')]")) != null,
				"Waited for popover for too long, it never showed up, gave up"
			);
		}

		public GridActionButton GetActionButton(string action)
		{
			return new GridActionButton(action);
		}
	}
}
