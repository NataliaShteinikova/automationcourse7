﻿using System;
using System.Collections.Generic;

using System.Linq;
using OpenQA.Selenium;

namespace AutomationCourseFramework.Pages.Archives
{
    public class ArchivesPage : BasePage
    {
        public ArchivesPage()
        {
        }

        public void LoadDirectly()
        {
            WebDriver.Navigate().GoToUrl("http://autotestcourse.fastdev.se/fotoweb/archives/");
        }

        public ArchivePageItem[] GetAllArchives()
        {
            WaitForThumbnailsLoadingComplited();

            var archiveElements = WebDriver.FindElements(By.XPath("//div[@id='thumbnailWrapperSimple']//ul[@class='archive-list media-sets']/li[@class='elem media-set-elem']"));
            var archiveItemsList = new List<ArchivePageItem>();
            for (int i = 0; i < archiveElements.Count; i++)
            {
                archiveItemsList.Add(new ArchivePageItem(i + 1));
            }

            return archiveItemsList.ToArray();
        }

        public ArchivePageItem GetArchive(string name)
        {

            //LINQ
            var item = GetAllArchives()
                            .FirstOrDefault(i => i.Name.Text == name);
            if (item == null)
                throw new ApplicationException($"Cannot get archive wiht name '{name}'");

            return item;
        }

        public void WaitForThumbnailsLoadingComplited()
        {
            WaitForCondition(() => WebDriver.FindElements(By.XPath("//div[@id='thumbnailWrapperSimple']//div[@class='loader' and @style='display: none;']")).Count > 0, "Archives loading timeout expired");
        }
    }
}
