﻿using AutomationCourseFramework.Pages.PageElements;
using AutomationCourseFramework.WebDriver;
using OpenQA.Selenium;

namespace AutomationCourseFramework.Pages.Archives
{
    public class ArchivePageItem : WebDriverBase
    {
        private string rootXpath;
        internal ArchivePageItem(int index)
        {
            rootXpath = $"//div[@id='thumbnailWrapperSimple']//ul[@class='archive-list media-sets']/li[@class='elem media-set-elem'][{index}]";
        }

        public Label Name => new Label(By.XPath($"{rootXpath}//div[@class='title']"));

        public void Open()
        {     
            WebDriver.FindElement(By.XPath($"{rootXpath}/a")).Click();
        }
    }
}
