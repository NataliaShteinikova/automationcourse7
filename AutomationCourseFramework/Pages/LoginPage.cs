﻿using AutomationCourseFramework.Pages.PageElements;
using AutomationCourseFramework.WebDriver;
using OpenQA.Selenium;

namespace AutomationCourseFramework.Pages
{
	public class LoginPage : BasePage
    {
		public LoginPage()
        {
        }

        public EditBox LoginOrEmail
        {
            get
            {
                return new EditBox(By.Id("username-page"));
            }
        }

        public EditBox Password
        {
            get
            {
                return new EditBox(By.Id("password-page"));
            }
        }

        public Button Login
        {
            get
            {
                return new Button(By.Id("page-form-submit"));
            }
        }
    }
}
