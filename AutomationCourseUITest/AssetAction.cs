﻿using System;
using System.Diagnostics;
using System.Threading;
using AutomationCourseFramework.Pages;
using AutomationCourseFramework.Pages.Archives;
using AutomationCourseFramework.Pages.Archives.Tab;
using AutomationCourseFramework.WebDriver;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AutomationCourseUITest
{
    [TestFixture]
	public class AssetAction : WebDriverBase
    {

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            LoginPage loginPage = new LoginPage();
            loginPage.LoginOrEmail.ClearAndKeyPress("natalia.shteinikova@fastdev.se");
            loginPage.Password.ClearAndKeyPress("test");
            loginPage.Login.Click();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            WebDriver.Quit();
        }

        [TearDown]
        public void TearDown()
        {
            WebDriver.Navigate().GoToUrl("http://autotestcourse.fastdev.se");
            LogoutFromFotoweb();
        }

        #region Auxiliary testing methods
        void LogoutFromFotoweb()
        {
            WebDriver.FindElement(By.XPath("//li[@id='userMenuButton']/a")).Click();
            WaitForUserMenulIsVisible();
            WebDriver.FindElement(By.XPath("//li[@id='userMenuButton']//div[@class='userMenuOption logout']")).Click();
        }

        public void WaitForActionPanelIsVisible()
        {
            WaitForCondition(() => WebDriver.FindElement(By.Id("grid-action-panel-wrapper")).
                GetAttribute("Class").Contains("grid-action-panel-open"),
                "Timeout of waiting for visibility of Action panel expired.");
            // Thread.Sleep(500);
        }

        public void WaitForUserMenulIsVisible()
        {
            WaitForCondition(() => WebDriver.FindElement(By.Id("userMenuButton")).
                GetAttribute("Class").Contains("header-size-element open"),
                "Timeout of waiting for UserMenu expired.");
            // Thread.Sleep(500);
        }

        public void WaitForStatusModalDialogIsVisible()
        {
            WaitForCondition(() => WebDriver.FindElement(By.XPath("//div[@class='modal in']")).
                GetAttribute("aria-hidden").Equals("false"),
                "Timeout of waiting for Status model dialog expired.");
            // Thread.Sleep(500);
        }

        public void WaitForRating(int rating)
        {
            WaitForCondition(() =>
            {
                var checkClassName = WebDriver.FindElement(By.XPath("//div[@id='preview-title']//span[contains(@class,'rating-info rate')]")).GetAttribute("class");
                var isEqual = $"rating-info rate-{rating}" == checkClassName;

                if (isEqual) return true;

                WebDriver.Navigate().Refresh();
                return false;

            }, "Timeout of setting Status expired.");
        }

        public void WaitForStatus(int status)
        {
            WaitForCondition(() =>
            {
                var checkClassName = WebDriver.FindElement(By.XPath("//div[@id='preview-title']//span[contains(@class,'js-status-bar status-bar status')]")).GetAttribute("class");
                var isEqual = $"js-status-bar status-bar status-{status}" == checkClassName;

                if (isEqual) return true;

                WebDriver.Navigate().Refresh();
                return false;

            }, "Timeout of setting Rating expired.");
        }

        /*public static void WaitForCondition(Func<bool> condition, string message)
        {
            WaitForCondition(condition, message, TimeSpan.FromSeconds(60), TimeSpan.FromMilliseconds(500));
        }

        public static void WaitForCondition(Func<bool> condition, string message, TimeSpan waitingTime, TimeSpan interval)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            while (!condition())
            {
                if (stopwatch.Elapsed.TotalSeconds > waitingTime.TotalSeconds)
                    throw new Exception(message);
                Thread.Sleep(interval);
            }
        }*/
        #endregion

        [Test]
        public void CheckRating()
        {
            const int rating = 3;

            var archivesPage = new ArchivesPage();
            archivesPage.LoadDirectly();
            var commonAssetsArchive = archivesPage.GetArchive("Common assets");
            commonAssetsArchive.Open();

            //Open Action panel for selected asset
            var archivesGridPage = new ArchiveGridPage();
            var archiveAsset = archivesGridPage.GetArchiveAsset("2014-05-02T070757Z_1460091317_GM1EA520KIQ01_RTRMADP_3_LEONE-SEACUCUMBERS.jpg");
            archiveAsset.Select();
			var gridActionPanel = new GridActionPanel();

			//Set rating = RATING (3))
			gridActionPanel.SetRating(rating);

			//Open detail page of the selected asset
			var assetDetailPage = archiveAsset.Open();
            Assert.AreEqual("http://autotestcourse.fastdev.se/fotoweb/archives/5000-Common%20assets/Test%20automation%20course/News%20agencies/2014-05-02T070757Z_1460091317_GM1EA520KIQ01_RTRMADP_3_LEONE-SEACUCUMBERS.jpg.info#c=%2Ffotoweb%2Farchives%2F5000-Common%2520assets%2F",
                WebDriver.Url, "URL of detail page is wrong");

            WebDriver.SwitchTo().Frame(WebDriver.FindElement(By.Id("preview-iframe")));
            WaitForRating(rating);

			//Check rating value
			var actualRating = assetDetailPage.Rating;
            Assert.AreEqual($"rating-info rate-{rating}", actualRating, "The assets has unexpected rating value.");

			//Clear rating
			assetDetailPage.Rating = 0;

            WebDriver.SwitchTo().ParentFrame();
        }

        [Test]
        public void CheckStatus()
        {
            const AssetState status = 3;

            //Open Action panel for selected asset
            var archivesGridPage = new ArchiveGridPage();
			var archiveAsset = archivesGridPage.GetArchiveAsset("2014-05-02T070757Z_1460091317_GM1EA520KIQ01_RTRMADP_3_LEONE-SEACUCUMBERS.jpg");
			archiveAsset.Select();
			var gridActionPanel = new GridActionPanel();

			//Set status = STATUS (3)
			gridActionPanel.SetStatus(AssetStatus.Three);

			//Open detail page of the selected asset
			var assetDetailPage = archiveAsset.Open();
			Assert.AreEqual("http://autotestcourse.fastdev.se/fotoweb/archives/5000-Common%20assets/Test%20automation%20course/News%20agencies/2014-05-02T070757Z_1460091317_GM1EA520KIQ01_RTRMADP_3_LEONE-SEACUCUMBERS.jpg.info#c=%2Ffotoweb%2Farchives%2F5000-Common%2520assets%2F",
                WebDriver.Url, "URL of detail page is wrong");

            WebDriver.SwitchTo().Frame(WebDriver.FindElement(By.Id("preview-iframe")));
            WaitForStatus(status);

            //Check status value
            var actualStatus = WebDriver.FindElement(By.XPath("//div[@id='preview-title']//span[contains(@class,'js-status-bar status-bar status')]")).GetAttribute("class");
            Assert.AreEqual($"js-status-bar status-bar status-{status}", actualStatus, "The assets has unexpected status value.");

            //Clear status
            WebDriver.FindElement(By.XPath("//div[@id='preview-title']//span[contains(@class,'js-status-bar status-bar status')]")).Click();
            WaitForStatusModalDialogIsVisible();
            WebDriver.FindElement(By.XPath("//div[@class='status-buttons modal-body']/button[@data-value='none']")).Click();

            //WebDriver.SwitchTo().ParentFrame();
        }

    }
}
